const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {
  const {
    createPage,
    createRedirect
  } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)

  return graphql(
    `
      {
        allContentfulFuncionario {
          edges {
            node {
              id
              name
              role
              image {
                id
              }
              displayOnWebsite
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }

    // Create blog posts pages.
    // const employees = result.data.allContentfulFuncionario.edges
    //
    // posts.forEach((post, index) => {
    //   const previous = index === posts.length - 1 ? null : posts[index + 1].node
    //   const next = index === 0 ? null : posts[index - 1].node
    //
    //   createPage({
    //     path: post.node.fields.slug,
    //     component: blogPost,
    //     context: {
    //       slug: post.node.fields.slug,
    //       previous,
    //       next,
    //     },
    //   })
    // })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField, createPage, deletePage } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

const replacePath = path => (path === `/` ? path : path.replace(/\/$/, ``))

exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions

  const oldPage = Object.assign({}, page)

  page.path = replacePath(page.path)
  if (page.path !== oldPage.path) {
    deletePage(oldPage)
    createPage(page)

    if (page.path === '/team') {
      page.path = '/equipe'

      createPage(page)
    }

    if (page.path === '/contact') {
      page.path = '/contato'

      createPage(page)
    }
  }

  if (page.path === '/') {
    createPage({
      ...page,
      path: '/sucesso'
    })
  }
}
