import React from 'react'

const HomeHero = () => (
  <div className="ui centered container stackable grid">
    <div className="row">
      <div className="eight wide column">
        <h1 className="ui inverted header">
          Nossa Equipe
        </h1>
      </div>
    </div>
  </div>
)

export default HomeHero
