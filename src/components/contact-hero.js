import React from 'react'

const ContactHero = () => (
  <div className="ui centered container stackable grid">
    <div className="row">
      <div className="eight wide column">
        <h1 className="ui inverted header">
          Como você prefere falar com a gente?
        </h1>
      </div>
    </div>
  </div>
)

export default ContactHero
