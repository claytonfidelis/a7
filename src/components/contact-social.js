import React from 'react'

const ContactSocial = ({
  instagram,
  facebook
}) => (
  <div class="ui a7 vertical basic stripe grey segment contact-social">
    <div class="ui stackable equal width grid container">

      <div class="eight wide column contact-method email">
        <h3 class="ui left aligned icon header">
          <i class="teal user plus icon"></i>

          <div class="content">Siga a A7</div>
        </h3>

        <p>Acompanhe o que tem de novo e saiba como usar a A7 da melhor forma.</p>

        <a class="social" href={instagram} target="blank">
          <i class="big instagram icon"></i>
        </a>

        <a class="social" href={facebook} target="blank">
          <i class="big facebook icon"></i>
        </a>
      </div>

    </div>
  </div>
)

export default ContactSocial
