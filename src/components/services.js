import React from 'react'

import { graphql, StaticQuery } from 'gatsby'
import Img from "gatsby-image"

const Services = () => (
  <StaticQuery
    query={query}
    render={data => (
      <div className="ui a7 grey vertical basic stripe segment services">
        <div className="ui middle aligned stackable equal width grid container">
          <div className="row">
            <div className="column">
              <h1 className="ui big centered header">Nossos serviços</h1>
            </div>
          </div>
          <div className="centered row">
            <div className="six wide column">
              <Img
                className="ui image"
                fluid={data.file.childImageSharp.fluid}
                style={{ minWidth: '400px' }}
              />
            </div>

            <div className="two wide column">
            </div>

            <div className="six wide column">
              <div className="ui very relaxed list">
                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Babá mensalista</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Babá folguista</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Babá Car</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Doméstica</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Cuidador de idoso</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Passeador de cães</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Click compras</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>ASG</p>
                  </div>
                </div>

                <div className="item">
                  <i className="green check large icon"></i>
                  <div className="content">
                    <p>Lavanderia</p>
                  </div>
                </div>

                {/* <!-- <div className="item"> --> */}
                {/*   <!--   <div className="content"> --> */}
                {/*     <!--     <p><a href="">Ver todos os serviços</a></p> --> */}
                {/*     <!--   </div> --> */}
                {/*   <!-- </div> --> */}
              </div>

            </div>
          </div>
        </div>
      </div>
    )}
  />
)

export default Services

const query = graphql`
  query {
    file(relativePath: { eq: "baby.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 4160) {
          ...GatsbyImageSharpFluid_noBase64
        }
      }
    }
  }
`
