import React from 'react'

const ContactMethods = ({
  email,
  phone,
  whatsapp
}) => (
  <div class="ui a7 vertical basic stripe grey segment">
    <div class="ui stackable equal width grid container">

      <div class="computer only column contact-method email">
        <h3 class="ui left aligned icon header">
          <i class="purple envelope icon"></i>

          <div class="content">
            E-mail

            <div class="sub header">Tem alguma dúvida?</div>
          </div>
        </h3>

        <p>{ email }</p>
      </div>

      <div class="column contact-method">
        <h3 class="ui left aligned icon header">
          <i class="blue phone icon"></i>

          <div class="content">
            Telefone

            <div class="sub header">Precisando é só chamar</div>
          </div>
        </h3>

        <p>{ phone }</p>
        <p class="more-info"><small>Das 07hs ás 19hs sem custo ou das 19:01 hs até ás 22:00hs, com custo adicional</small></p>
      </div>

      <div class="column contact-method">
        <h3 class="ui left aligned icon header">
          <i class="green whatsapp icon"></i>

          <div class="content">
            Whatsapp

            <div class="sub header">Precisa de uma ajuda agora?</div>
          </div>
        </h3>

        <p>{ whatsapp }</p>
      </div>

      <div class="sixteen wide tablet only mobile only column contact-method email">
        <h3 class="ui left aligned icon header">
          <i class="purple envelope icon"></i>

          <div class="content">
            E-mail

            <div class="sub header">Tem alguma dúvida?</div>
          </div>
        </h3>

        <p>{ email }</p>
      </div>

    </div>
  </div>
)

export default ContactMethods
