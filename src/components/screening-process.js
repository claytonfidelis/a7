import React from 'react'

import { graphql, StaticQuery } from 'gatsby'
import BackgroundImage from 'gatsby-background-image'

const ScreeningProcess = () => (
  <StaticQuery
    query={query}
    render={data => {
      return (
        <div className="ui inverted vertical basic stripe segment screening">
          <div className="ui aligned stackable equal width centered grid container">
            <div className="row">
              <div className="column">
                <h1 className="ui inverted big centered header">Processo de seleção</h1>
              </div>
            </div>
            <div className="row screening-item">
              <div className="six wide centered column">
                <h3 className="ui inverted icon step header">
                  <i className="yellow map pin icon"></i>

                  <div className="content">
                    Encontro
                  </div>
                </h3>
                <p>Todos os profissionais da A7 Home Service se apresentam no nosso escritório para participar de uma entrevista e verificação de todos os documentos</p>
              </div>

              <div className="six wide centered column">
                <h3 className="ui inverted icon step header">
                  <i className="yellow clipboard list icon"></i>

                  <div className="content">Formulário</div>
                </h3>
                <p>Todos os candidatos respondem um extenso questionário para que possamos compreendê-los melhor.</p>
              </div>
            </div>

            <div className="row screening-item">
              <div className="six wide centered column">
                <h3 className="ui inverted icon step header">
                  <i className="yellow mini filter icon"></i>

                  <div className="content">Triagem</div>
                </h3>
                <p>Com base na entrevista e na aplicação do formulário, selecionamos os profissionais que mais se encaixam no perfil da A7 Home Service.</p>
              </div>

              <div className="six wide centered column">
                <h3 className="ui inverted step icon header">
                  <i className="yellow book icon"></i>

                  <div className="content">Treinamento</div>
                </h3>
                <p>Os profissionais que se encaixam no nosso perfil participam do nosso treinamento.</p>
              </div>

            </div>

          </div>

          <BackgroundImage
            Tag="div"
            className="background"
            classId="screening"
            fluid={data.file.childImageSharp.fluid}
            backgroundColor="#0d3641"
          />
        </div>
      )
    }}
  />
)

export default ScreeningProcess

const query = graphql`
  query {
    file(relativePath: { eq: "selecao.jpg"}) {
      childImageSharp {
        fluid(quality: 100, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`
