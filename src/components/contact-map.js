import React from 'react'

import { compose, withProps } from 'recompose'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

const ContactMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBm8ZsH2CyVOXeHjZaPf-ppx787rcxPULU",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(({
  latitude,
  longitude
}) => (
  <GoogleMap
    defaultZoom={16}
    defaultCenter={{
      lat: latitude,
      lng: longitude
    }}
  >
    <Marker
      position={{
        lat: latitude,
        lng: longitude
      }}
    />
  </GoogleMap>
))

export default ContactMap
