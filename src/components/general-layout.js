import React from "react"

import Footer from './footer'
import Masthead from './masthead'
import FooterCTA from './footer-cta'

class Layout extends React.Component {
  render() {
    const { children } = this.props

    return (
      <div className="pusher">
        <Masthead>
        </Masthead>

        {children}

        <FooterCTA />
        <Footer />
      </div>
    )
  }
}

export default Layout
