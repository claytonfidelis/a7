import React from 'react'

const Steps = () => (
  <div className="ui vertical basic stripe segment">
    <div className="ui aligned stackable equal width grid container steps-about">
      <div className="row">
        <div className="column">
          <h1 className="ui big centered header">A7 em 3 passos</h1>
        </div>
      </div>
      <div className="row">
        <div className="column">
          <h3 className="ui step header">
            <span className="step">1</span>

            <div className="content">
              Contato
            </div>
          </h3>
          <p>Entre em contato conosco através do nosso site, redes sociais, e-mail ou telefone e informe o que você precisa</p>
        </div>

        <div className="column">
          <h3 className="ui step header">
            <span className="step">2</span>

            <div className="content">Encontro</div>
          </h3>
          <p>Agendaremos um encontro, para disponibilizar o banco de profissionais cadastrados.</p>
        </div>

        <div className="column">
          <h3 className="ui step header">
            <span className="step">3</span>

            <div className="content">Contratação</div>
          </h3>
          <p>Assine o tipo de contrato que melhor lhe atende e agendaremos o dia e horário para receber o profissional A7 na comodidade da sua casa.</p>
        </div>

      </div>
    </div>
  </div>
)

export default Steps
