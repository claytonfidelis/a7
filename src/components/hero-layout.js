import React from "react"

import Footer from './footer'
import Masthead from './masthead'
import FooterCTA from './footer-cta'

class Layout extends React.Component {
  render() {
    const {
      children,
      location,
      hero,
      bgImage,
      contactInfo
    } = this.props

    return (
      <div
        className="pusher"
        id={getPageName(location)}
      >
        <Masthead
          location={location}
          bgImage={bgImage}
        >
          { hero }
        </Masthead>

        { children }

        <FooterCTA {...contactInfo} />
        <Footer {...contactInfo} />
      </div>
    )
  }
}

function getPageName(location) {
  switch (location.pathname) {
    case '/': return 'home'
    case '/equipe': return 'team'
    case '/contato': return 'contact'
    default: return ''
  }
}

export default Layout
