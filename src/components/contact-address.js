import React from 'react'

import ContactMap from './contact-map'

const ContactAddress = ({
  address,
  latitude,
  longitude
}) => (
  <div class="ui a7 vertical basic stripe segment">
    <div class="ui middle aligned stackable grid container">
      <div class="sixteen wide centered column">
        <h1 class="ui centered big header">
          Endereço
          <div class="sub header">{ address }</div>
        </h1>
      </div>
      <div id="map">
        <ContactMap
          latitude={latitude}
          longitude={longitude}
        />
      </div>
    </div>
  </div>
)

export default ContactAddress
