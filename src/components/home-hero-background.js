import React from 'react'

import { graphql, StaticQuery } from 'gatsby'
import BackgroundImage from 'gatsby-background-image'

const HomeHeroBackground = () => (
  <StaticQuery
    query={bgQuery}
    render={data => (
      <BackgroundImage
        Tag="div"
        className="hero image"
        classId="hero-image"
        fluid={data.file.childImageSharp.fluid}
        backgroundColor="#0d3641"
      />
    )}
  />
)

export default HomeHeroBackground

const bgQuery = graphql`
  query {
    file(relativePath: { eq: "hero-image.jpg"}) {
      childImageSharp {
        fluid(quality: 100, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`
