import React from 'react'

import BackgroundImage from 'gatsby-background-image'

const TeamMember = ({
  id,
  name,
  image,
  role,
}) => (
  <div className="ui card employee">
    <div className="image">
      <BackgroundImage
        Tag="div"
        className="bg"
        fluid={image.fluid}
        backgroundColor="#0d3641"
      />
    </div>
    <div className="content">
      <div className="header">{ name }</div>
      {/* <div className="meta"> */}
      {/*   <a>{ role.join(', ') }</a> */}
      {/* </div> */}
    </div>
  </div>
)

export default TeamMember
