import React from 'react'

const About = () => (
  <div className="ui a7 vertical basic stripe grey segment">
    <div className="ui centered stackable grid container reasons">
      <div className="row">
        <div className="ten wide column motivation">
          <h1 className="ui big centered header">Por que a A7?</h1>
          <h3 className="ui centered thin header">Porque não somos apenas uma empresa. Somos a extensão da sua casa, da sua família e do seu dia a dia.</h3>
        </div>
      </div>

      <div className="row">
        <div className="centered seven wide column">
          <i className="big yellow lightbulb outline icon"></i>

          <h2 className="ui centered thin header">Utilizamos nosso know-how na seleção de profissionais domésticos e oferecemos aos nossos clientes, a tranquilidade de saber que cada profissional foi devidamente checado e validado.</h2>
        </div>

        <div className="centered seven wide column">
          <i className="big yellow paw icon"></i>

          <h2 className="ui centered thin header">Cuidamos ainda do seu animal de estimação, tão importante quanto um membro da família. Fazemos suas compras de supermercado. Levamos sua roupa na lavanderia. </h2>
        </div>
      </div>

      <div className="row">
        <div className="centered sixteen wide column">
          <i className="big yellow star outline icon"></i>

          <h2 className="ui centered thin header">Cuidar de você e de sua casa. Otimizar seu tempo. <br />Esse é nossa meta!</h2>
        </div>
      </div>
    </div>
  </div>
)

export default About
