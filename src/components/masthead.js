import React from 'react'

import Menu from './menu'

const Masthead = ({ bgImage, children, location }) => console.log(location) || (
  <div className="ui inverted vertical masthead center aligned segment">
    <Menu active={location.pathname} />

    { children }

    { bgImage }
  </div>
)

export default Masthead
