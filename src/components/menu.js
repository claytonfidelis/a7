import React from 'react'

import { Link } from 'gatsby'

import classNames from 'classnames'

const Menu = ({
  active
}) => (
  <div className="ui container">
    <div className="ui large secondary inverted pointing menu">
      <Link
        className={classNames('item', { active: active === '/' })}
        to="/"
      >
        Inicio
      </Link>
      <Link
        className={classNames('item', { active: active === '/equipe' })}
        to="/equipe"
      >
        Nossa Equipe
      </Link>
      <Link
        className={classNames('item', { active: active === '/contato' })}
        to="/contato"
      >
        Contato
      </Link>
    </div>
  </div>
)

export default Menu
