import React from 'react'

const Footer = ({
  email,
  instagram,
  facebook
}) => (
  <footer className="ui vertical footer segment">
    <div className="ui container">
      <div className="ui equal width stackable grid">
        <div className="column">
          <a className="social" href={facebook} target="blank">
            <i className="big facebook icon"></i>
          </a>

          <a className="social" href={instagram} target="blank">
            <i className="big instagram icon"></i>
          </a>
        </div>

        <div className="right aligned column">
          <div className="row">
            <a
              className="unstyled"
              href={`mailto:${email}`}
            >
              { email }
            </a>
          </div>

          <div className="row">
            © 2019 A7 Home Service
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
