import React from 'react'

import { graphql, StaticQuery } from 'gatsby'
import BackgroundImage from 'gatsby-background-image'

const FooterCTA = ({
  whatsapp,
  phone,
  email
}) => {
  const phoneClear = phone.replace(/[^0-9.]/g, "")
  const whatsappClear = whatsapp.replace(/[^0-9.]/g, "")

  return (
    <StaticQuery
      query={query}
      render={data => {
        return (
          <section className="ui vertical basic stripe segment footer-cta">
            <div className="ui aligned stackable equal width grid container">
              <div className="row">
                <div className="right floated six wide column">
                  <h1 className="ui inverted big header">
                    Estamos prontos para cuidar do que for preciso, e você?
                  </h1>

                  {/* <button className="ui white button">Quero fazer parte da família A7</button> */}

                  <div className="contact">
                    <a
                      className="contact-method"
                      href={`https://api.whatsapp.com/send?phone=55${whatsappClear}&text=&source=website&data=`}
                    >
                      <i className="whatsapp icon"></i> { whatsapp }
                    </a>

                    <a
                      className="contact-method"
                      href={`tel:+55${phoneClear}`}
                    >
                      <i className="phone icon"></i> { phone }
                    </a>

                    <a
                      className="contact-method"
                      href={`mailto:${email}`}
                    >
                      <i className="envelope outline icon"></i> { email }
                    </a>
                  </div>
                </div>
              </div>

              <BackgroundImage
                Tag="div"
                className="background"
                classId="footer"
                fluid={data.file.childImageSharp.fluid}
                backgroundColor="#0d3641"
              />
            </div>
          </section>
        )
      }}
    />
  )
}

const query = graphql`
  query {
    file(relativePath: { eq: "footer.jpg"}) {
      childImageSharp {
        fluid(quality: 100, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`

export default FooterCTA
