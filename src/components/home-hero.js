import React from 'react'

const HomeHero = ({ location }) => (
  <div className="ui container stackable grid">
    <div className="row">
      <div className="eight wide column">
        <h1 className="ui inverted header">
          A7 Home Service
          <h2 className="ui inverted thin header">Somos a extensão da sua casa, da sua família e do seu dia a dia</h2>
        </h1>
      </div>
      <div className="six wide right floated column">
        <div className="ui mini form left aligned segment">
          <h1 className="ui bold header">
            Fale conosco
            <div className="sub header">Preencha o formulário e entraremos em contato</div>
          </h1>

          <form
            id="contact-form"
            name="contact"
            method="POST"
            data-netlify="true"
            action="/sucesso"
          >
            <input
              type="hidden" name="form-name" value="contact" />

            <p>
              <label>
                Nome:
                <input
                  type="text"
                  name="name"
                  required
                />
              </label>
            </p>
            <p>
              <label>
                E-mail:
                <input
                  type="email"
                  name="email"
                />
              </label>
            </p>
            <p>
              <label>
                Telefone:
                <input
                  type="tel"
                  name="phone"
                  required
                />
              </label>
            </p>
            <p>
              <label>
                Mensagem:
                <textarea
                  name="message"
                  rows="4"
                  required
                />
              </label>
            </p>

            <div className="ui positive message" id="contact-success">
              <p>Recebemos sua solicitação, em breve entraremos em contato.</p>
            </div>

            { location.pathname === '/sucesso' && (
              <div class="ui positive message">
                Recebemos seus dados, em breve entraremos em contato.
              </div>
            )}

            <div>
              <button
                type="submit"
                className="ui primary fluid button"
              >
                Quero saber mais!
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
)

export default HomeHero
