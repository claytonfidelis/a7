import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/hero-layout"
import SEO from "../components/seo"
import TeamHero from '../components/team-hero'
import TeamHeroBackground from '../components/team-hero-background'
import TeamMember from '../components/team-member'

class Team extends React.Component {
  normalizeEmployees (employees) {
    let normalized = {}

    employees.forEach(employee => {
      employee.node.role.forEach(role => {
        if (normalized[role]) {
          normalized[role].push(employee)
        } else {
          normalized[role] = [ employee ]
        }
      })
    })

    return normalized
  }

  renderTeam (team) {
    const teamRoles = Object.keys(team)

    return teamRoles.map((roleName, index) => {
      const employeesWithThisRole = team[roleName]
      const roleSlug = roleName.toLowerCase().split(' ').join('-')
      const indexMod = index % 2
      const isGrey = indexMod === 0 ? 'grey' : ''

      return (
        <div
          className={`ui a7 vertical basic stripe ${isGrey} segment`}
          key={roleSlug}
        >
          <div className="ui stackable grid container members">
            <div className="row">
              <div className="column">
                <h1 className="ui big header">{ roleName }</h1>
              </div>
            </div>

            <div className="row">
              { this.renderEmployees(employeesWithThisRole, roleSlug) }
            </div>
          </div>
        </div>
      )
    })
  }

  renderEmployees (employees = [], roleSlug = '') {
    return employees.map(employee => (
      <div
        className="four wide column"
        key={`${roleSlug}:${employee.node.id}`}
      >
        <TeamMember {...employee.node} />
      </div>
    ))
  }

  render () {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const employees = data.allContentfulFuncionario.edges
    const normalizedEmployees = this.normalizeEmployees(employees)
    const contactInfo = data.contactInfo.edges[0].node

    return (
      <Layout
        location={this.props.location}
        bgImage={<TeamHeroBackground />}
        title={siteTitle}
        hero={<TeamHero />}
        contactInfo={contactInfo}
      >
        <SEO
          title="Equipe - A7 Home Service"
          keywords={[`a7`, `home service`, `baba`, `asg`, `diarista`, `lavanderia`]}
        />

        { this.renderTeam(normalizedEmployees) }
      </Layout>
    )
  }
}

export default Team

export const pagequery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulFuncionario(sort: { order: ASC, fields: name }) {
      edges {
        node {
          id
          name
          role
          image {
            fluid {
              ...GatsbyContentfulFluid
            }
          }
          displayOnWebsite
        }
      }
    }
    contactInfo: allContentfulPaginaDeContato {
      edges {
        node {
          id
          whatsapp
          phone
          email
          facebook
          instagram
          address
          latitude
          longitude
        }
      }
    }
  }
`
