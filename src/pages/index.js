import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/hero-layout"
import SEO from "../components/seo"

import About from '../components/about'
import Steps from '../components/steps'
import ScreeningProcess from '../components/screening-process'
import Services from '../components/services'
import HomeHero from '../components/home-hero'
import HomeHeroBackground from '../components/home-hero-background'

class BlogIndex extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const contactInfo = data.contactInfo.edges[0].node

    return (
      <Layout
        location={this.props.location}
        title={siteTitle}
        bgImage={<HomeHeroBackground />}
        hero={<HomeHero location={this.props.location} />}
        contactInfo={contactInfo}
      >
        <SEO
          title="A7 Home Service"
          keywords={[`a7`, `home service`, `baba`, `asg`, `diarista`, `lavanderia`]}
        />

        <About />
        <Steps />
        <ScreeningProcess />
        <Services />
      </Layout>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    contactInfo: allContentfulPaginaDeContato {
      edges {
        node {
          id
          whatsapp
          phone
          email
          facebook
          instagram
          address
          latitude
          longitude
        }
      }
    }
  }
`
