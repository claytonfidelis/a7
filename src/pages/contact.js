import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/hero-layout"
import SEO from "../components/seo"
import ContactSocial from '../components/contact-social'
import ContactMethods from '../components/contact-methods'
import ContactHero from '../components/contact-hero'
import ContactHeroBackground from '../components/contact-hero-background'
import ContactAddress from '../components/contact-address'

class Team extends React.Component {
  render () {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const contactInfo = data.contactInfo.edges[0].node

    return (
      <Layout
        location={this.props.location}
        bgImage={<ContactHeroBackground />}
        title={siteTitle}
        hero={<ContactHero />}
        contactInfo={contactInfo}
      >
        <SEO
          title="Contato - A7 Home Service"
          keywords={[`a7`, `home service`, `baba`, `asg`, `diarista`, `lavanderia`]}
        />

        <ContactMethods {...contactInfo} />
        <ContactSocial {...contactInfo} />
        <ContactAddress {...contactInfo} />
      </Layout>
    )
  }
}

export default Team

export const pagequery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulFuncionario {
      edges {
        node {
          id
          name
          role
          image {
            fluid {
              ...GatsbyContentfulFluid
            }
          }
          displayOnWebsite
        }
      }
    }
    contactInfo: allContentfulPaginaDeContato {
      edges {
        node {
          id
          whatsapp
          phone
          email
          facebook
          instagram
          address
          latitude
          longitude
        }
      }
    }
  }
`
