import React from "react"
import { graphql } from "gatsby"

class NotFoundPage extends React.Component {
  render() {
    return (
      <h1>Not Found</h1>
    )
  }
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
